package nl.bioinf.zaverstappen.SpeciesBrowser.servlets;
import nl.bioinf.zaverstappen.SpeciesBrowser.config.WebConfig;
import nl.bioinf.zaverstappen.SpeciesBrowser.models.*;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@WebServlet(name = "SpeciesServlet", urlPatterns = "/species.detail")
public class SpeciesServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    History history = new History();

    @Override
    public void init() throws ServletException {
        this.templateEngine = WebConfig.createTemplateEngine(getServletContext());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String catSpecies = request.getParameter("cat_species");
        String speciesFile = getServletContext().getInitParameter("speciesFilePath");
        final Map<String, Cat> cats = CSVReader.fileReader(speciesFile);
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        HttpSession session = request.getSession();
        String scientificName = catSpecies.replace("_", " ");
        if (session.isNew()){
            session.setAttribute("history",history.getHistory());
        } else {
            history.addItem(cats.get(scientificName).getEnglishName());
            session.setAttribute("history", history.getHistory());
        }
        String language = locale.getLanguage();
        if(locale.getLanguage().equals("nl")){
            ctx.setVariable("name", cats.get(scientificName).getDutchName());
            ctx.setVariable("other_name", cats.get(scientificName).getEnglishName());
        } else {
           ctx.setVariable("name", cats.get(scientificName).getEnglishName());
           ctx.setVariable("other_name", cats.get(scientificName).getDutchName());
        }
        ctx.setVariable("scientific_name", scientificName);
        ctx.setVariable("picture", cats.get(scientificName).getPicture());
        ctx.setVariable("size", cats.get(scientificName).getSize());
        ctx.setVariable("weight", cats.get(scientificName).getWeight());
        ctx.setVariable("catSpecies", catSpecies);
        templateEngine.process("Species", ctx, response.getWriter());
        }
    }
