package nl.bioinf.zaverstappen.SpeciesBrowser.models;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class History {
    private static final int MAX_SIZE = 5;
    private Queue<String> historyItems = new LinkedList();

    public void addItem(String resource) {
        if (historyItems.size() == MAX_SIZE) {
            historyItems.remove();
        }
        historyItems.add(resource);
    }

    public List<String> getHistory() {
        //return history
        return (List)this.historyItems;
    }
}
