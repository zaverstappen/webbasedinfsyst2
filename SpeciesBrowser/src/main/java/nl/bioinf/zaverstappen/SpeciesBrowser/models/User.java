package nl.bioinf.zaverstappen.SpeciesBrowser.models;

import java.util.ArrayList;
import java.util.List;

public class User {
    private final String name;
    private final String password;
    private Role role;

    public enum Role {
        ADMIN,
        USER,
        GUEST
    }

    public User(String name, String password, Role role){
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public static List<User> getSome() {
        List<User> users = new ArrayList<>();
        User u;
        u = new User("Henk", "henk", Role.ADMIN);
        users.add(u);
        u = new User("Roger", "roger", Role.USER);
        users.add(u);
        u = new User("Diana", "diana", Role.GUEST);
        users.add(u);

        return users;
    }

}
