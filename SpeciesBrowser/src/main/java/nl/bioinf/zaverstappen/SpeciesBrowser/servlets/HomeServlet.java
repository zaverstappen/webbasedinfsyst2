package nl.bioinf.zaverstappen.SpeciesBrowser.servlets;
import nl.bioinf.zaverstappen.SpeciesBrowser.config.WebConfig;
import nl.bioinf.zaverstappen.SpeciesBrowser.models.CSVReader;
import nl.bioinf.zaverstappen.SpeciesBrowser.models.Cat;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;

@WebServlet(name = "HomeServlet", urlPatterns = "/home")
public class HomeServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        this.templateEngine = WebConfig.createTemplateEngine(getServletContext());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String speciesFile = getServletContext().getInitParameter("speciesFilePath");
        final Map<String, Cat> cats = CSVReader.fileReader(speciesFile);
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        HttpSession session = request.getSession();
        ctx.setVariable("cats", cats);
        templateEngine.process("species-listing", ctx, response.getWriter());

    }
}

