package nl.bioinf.zaverstappen.SpeciesBrowser.servlets;
import nl.bioinf.zaverstappen.SpeciesBrowser.config.WebConfig;
import nl.bioinf.zaverstappen.SpeciesBrowser.models.CSVReader;
import nl.bioinf.zaverstappen.SpeciesBrowser.models.Cat;
import nl.bioinf.zaverstappen.SpeciesBrowser.models.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        this.templateEngine = WebConfig.createTemplateEngine(getServletContext());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        HttpSession session = request.getSession();
        templateEngine.process("login", ctx, response.getWriter());

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        HttpSession session = request.getSession();
        String nextPage;

        if (session.isNew() || session.getAttribute("user") == null) {
            boolean authenticated = authenticate(username, password);
            if (authenticated) {
                session.setAttribute("user", new User("Henk", "henk", User.Role.ADMIN));
                nextPage = "species-listing";
            } else {
                ctx.setVariable("message", "Your password and/or username are incorrect; please try again");
                ctx.setVariable("message_type", "error");
                nextPage = "login";
            }
        } else {
            nextPage = "species-listing";
        }
        templateEngine.process(nextPage, ctx, response.getWriter());

    }

    private boolean authenticate(String username, String password) {
        List<User> users = User.getSome();
        System.out.println(users.toString());
        //for user in list, get list first name, if not in false, if in check password
        return username.equals("Henk") && password.equals("henk");
    }
}
