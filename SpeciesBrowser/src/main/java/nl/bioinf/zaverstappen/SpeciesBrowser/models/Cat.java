package nl.bioinf.zaverstappen.SpeciesBrowser.models;

public class Cat{
    private final String ScientificName;
    private final String EnglishName;
    private final String DutchName;
    private final Integer Weight;
    private final Integer Size;
    private final String Picture;


    public Cat(String ScientificName, String EnglishName, String DutchName, Integer Weight, Integer Size, String Picture){
        this.ScientificName = ScientificName;
        this.EnglishName = EnglishName;
        this.DutchName = DutchName;
        this.Weight = Weight;
        this.Size = Size;
        this.Picture = Picture;
    }

    public String getScientificName() {
        return ScientificName;
    }

    public String getEnglishName() {
        return EnglishName;
    }

    public String getDutchName() {
        return DutchName;
    }

    public Integer getWeight() {
        return Weight;
    }

    public Integer getSize() {
        return Size;
    }

    public String getPicture() {
        return Picture;
    }
}
